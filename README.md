# Kubernetes

## Nedir?

* Tanımlar

**node** : düğüm
**container**: kap
**pod**: koza
**cluster**: küme

### Yetenekleri 

**servis keşfi ve yükdengeleme**

Kubernetes, bir kabı DNS adını veya kendi IP adreslerini kullanarak dış dünyaya açar. Bir kaba giden trafik yüksekse, Kubernetes uygulamanın kararlı olması için sayıyı artırır ve yükün dağılmasını sağlar.
    
**Depolama yönetimi**

Kubernetes, yerel depolar, genel bulut sağlayıcıları ve daha fazlası gibi istediğiniz bir depolama sistemini otomatik olarak monte etmenizi sağlar.
    
**otomatik sürümleme ve gerialma**

Kubernetes kullanarak konuşlandırılan kaplarınızın istenen durumunu tanımlayabilirsiniz ve gerçek durumu denetlenen bir hızda istenen duruma değiştirebilir. Örneğin, dağıtımınız için yeni kaplar oluşturmak, mevcut kapları kaldırmak ve bunların tüm kaynaklarını yeni kaplara aktarmak işini Kubernetes ile otomatikleştirebilirsiniz.

**otomatik uygulama kutulama**

Kubernetes'e, kaplaştırılmış görevleri çalıştırmak için kullanabileceği bir nod kümesi sağlarsınız. Kubernetes'e her bir kap için ne kadar CPU ve bellek (RAM) gerektiğini söylersiniz. Kubernetes, kaynaklarınızdan en iyi şekilde yararlanmak için kaplarınızı düğümlere yerleştirebilir.

**kendini iyileştirme**

Kubernetes, başarısız olan kapları yeniden başlatır, kapları değiştirir, kullanıcı tanımlı sağlık kontrolünüze yanıt vermeyen kapları öldürür ve hizmet vermeye hazır oluncaya kadar onları istemcilere açmaz.

**gizler ve yapılandırma yönetimi**

Kubernetes, parolalar, OAuth belirteçleri ve SSH anahtarları gibi hassas bilgileri depolamanızı ve yönetmenizi sağlar. kap görüntülerinizi yeniden oluşturmadan ve küme yapılandırmanızdaki gizli bilgileri açığa çıkarmadan, gizleri ve uygulama yapılandırmasını dağıtabilir ve güncelleyebilirsiniz. 

## Yapısı 
## Master Node
#### kube-apiserver
- Herşeyin arayüzüdür. 

#### kube-scheduler


#### kube-controller-manager

**namespace-controller**

**deployment-controller**

**replicaset-controller**




## Worker Node

### kubelet

kube-apiserver ı dinler. 
konteynerlerin yaratılmasını sağlar. 
container-runtime ile konuşarak yapar.

### container-runtime 

docker
rkt

### kube-proxy 

network kurallarını uygular ve yaşatır.

## Kavramlar

### Pods
### Replication Sets
### Deployment
### serviceaccount
### Namespace 
### Services

Birden çok portu dinleyip alttaki podlara adresleyebilir.
Hizmet ettiği podları adreslemek için `selector` kullanır.  
* type: `LoadBalancer`, `NodePort`, `ClusterIP`(default)
    * ClusterIP: podlar geçici olduğundan podlar podlarla ya pod kümeleriyle bun ip üzerinden konuşur. Küme dışından erişilemez.
    * NodePort: Küme dışından erişim için gereklidir. nodun ip adresinin bir portuna (30000-32000 arası) servisi eşleştirir. Her nod için yapar bunu ve NodePort ikilisini bilirsek tüm nodelardaki servise dışardan tek adresten erişebilir. Bu çirkin bir yöntemdir. 
    * LoadBalancer: nodeport / servis işini kubernetes dışında tutar. 

### Endpoints

### ConfigMaps
### Secrets
### Ingres

* hostport: docker tarzı port eşleme
* nodeport: nodtaki servise gidiyor, o yine load balance ediyor. 


### Loadbalancer
### NodePort
### Proxy
### Labels
### cni

### rkt

* kubelet doğrudan pod üretir. (?)
* eğer rkt kullanılırsa `cni` rkt içinde native olarak geliyor. `kubelet`e ihtiyaç yok.
* rkt kvm ile vmleştirmede sağlar.

## Network

* Her podun bir ip adresi vardır. Kümedeki her pod diğeriyle bu ip üzerinden konuşabilir.
* Nodlarda podlarla doğrudan nat olmadan konuşabilir.
* Pod içerisindeki konteynerler birbirleriyle localhost üzerinden konuşurlar.
* nodlar arasındaki trafiğin yönlendirilmesini en altta `iptables` yönetir. 

## kubectl komutları

    create
    deploy
    describe
    service
    expose
    log

## Linux namespaces
## cgroups
## union filesystems


## Kubernetes Deneyin

[Katacoda](https://www.katacoda.com/courses/kubernetes/playground)
[Play with Kubernetes](https://www.katacoda.com/courses/kubernetes/playground)